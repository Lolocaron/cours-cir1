<html>
    <head>
    	<meta charset="utf-8">
        <link rel="stylesheet" href="connect.css" media="screen" type="text/css" />
        <title>Connect</title>
    </head>
    <body>
        <div id="container">
            <form action="connection.php" method="post">
                <h1>Connect</h1>
                
                <label><b>Username</b></label>
                <input type="text" placeholder="Enter username" name="username" required>

                <label><b>Password</b></label>
                <input type="password" placeholder="Enter password" name="password" required>
                <?php 
                    session_start();
                    if(!empty($_SESSION['error'])){
                        echo "<div class=\"error\">";
                        echo htmlspecialchars($_SESSION['error']);
                        echo "</div>"; 
                        $_SESSION['error'] = "";
                    }
                 ?>
                <input type="submit" id='submit' value='Login' >
            </form>
        </div>
    </body>
</html>