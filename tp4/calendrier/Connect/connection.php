<?php
session_start();

//it checks if the username field is empty, if it is empty it print an error message
if(empty($_POST['username'])) {
    echo "Username field is empty.";
} 
else {
    //now it checks if the password field is empty
    if(empty($_POST['password'])) {
        echo "Password field is empty.";
    } 
    else {
        
        //if the password field and username field are not empty
        $username = htmlentities($_POST['username'], ENT_QUOTES, "ISO-8859-1");  
        $MotDePasse = htmlentities($_POST['password'], ENT_QUOTES, "ISO-8859-1");

        //connection to the data base and it checks if the password and the username are correct
        try{
            $bdd = new PDO('mysql:host=localhost;dbname=calendar;charset=utf8','root', 'isencir');
        }catch(Exception $e){
            exit("Erreur" .$e -> getMessage());
        }

        $result = $bdd->prepare("SELECT * FROM Users WHERE login = ?");
        $result ->execute(array($username));
        $user = $result->fetch();

        //password and username correct
        if(password_verify($MotDePasse, $user['password'])){
            $_SESSION['username'] = $username;
            header("Location: ../cal.php");
            }
        //password and username are false
        else{
            $_SESSION['error'] = "Mot de passe incorrect";
            header("Location: Connect.php");
        
        }
    }
}

?>